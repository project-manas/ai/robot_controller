cmake_minimum_required(VERSION 2.8.3)
project(robot_controller)

add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
  nav_msgs
  pluginlib
  roscpp
  serial
  serial_parser
  std_msgs
)

catkin_package(
 INCLUDE_DIRS include
 LIBRARIES ${PROJECT_NAME}
 CATKIN_DEPENDS nav_msgs pluginlib roscpp serial serial_parser std_msgs
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

add_library(odometry_model_plugins odometry_model_plugins/differential_model.cpp)
add_library(controller_plugins controller_plugins/diff_drive_controller.cpp)
add_library(${PROJECT_NAME} src/${PROJECT_NAME}.cpp)
add_executable(${PROJECT_NAME}_node src/${PROJECT_NAME}_node.cpp)
target_link_libraries(${PROJECT_NAME}_node ${PROJECT_NAME} ${catkin_LIBRARIES})

install(TARGETS ${PROJECT_NAME} ${PROJECT_NAME}_node
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
)

install(FILES controller_plugins.xml odometry_model_plugins.xml
        DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION})

install(DIRECTORY include/${PROJECT_NAME}/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
  FILES_MATCHING PATTERN "*.h"
  PATTERN ".svn" EXCLUDE
)

