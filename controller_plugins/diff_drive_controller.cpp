/*************************************
*  Copyright (c) 2019, Project MANAS
*  Author: Shrijit Singh
*************************************/

#include "pluginlib/class_list_macros.h"
#include "controller/diff_drive_controller.h"

PLUGINLIB_EXPORT_CLASS(diff_drive_controller::DifferentialDriveController, controller::Controller)

using namespace diff_drive_controller;

void DifferentialDriveController::initialize(ros::NodeHandle *nh, ros::NodeHandle *private_nh) {
  nh_ = nh;
  private_nh_ = private_nh;
  wheel_effort_ = wheel_data();
  private_nh_->param("upper_effort_limit", upper_effort_limit_, 1.0f);
  private_nh_->param("lower_effort_limit", lower_effort_limit_, -1.0f);

  setup_pid_controller();
}

void DifferentialDriveController::setup_pid_controller() {
  left_wheel_state_pub_= nh_->advertise<std_msgs::Float64>("left_wheel/state", 1);
  right_wheel_state_pub_ = nh_->advertise<std_msgs::Float64>("right_wheel/state", 1);
  left_wheel_control_sub_ = nh_->subscribe("/left_wheel/control_effort", 1, &DifferentialDriveController::left_wheel_control_callback, this);
  right_wheel_control_sub_ = nh_->subscribe("/right_wheel/control_effort", 1, &DifferentialDriveController::right_wheel_control_callback, this);
}

void DifferentialDriveController::get_effort(std::string data, uint8_t *buffer, size_t buffer_length, ros::Time last_update_time) {
  deserialize(data);
  publish_state(wheel_velocity_);
  serialize(wheel_effort_, buffer, buffer_length);
}

void DifferentialDriveController::deserialize(std::string data) {
  // Ensure endianness is handled
  std::memcpy(&wheel_velocity_, data.c_str(), 8);
}

void DifferentialDriveController::serialize(wheel_data control_effort, uint8_t *buffer, size_t buffer_length) {
  std::memcpy(buffer, &control_effort, buffer_length);
}

float DifferentialDriveController::clamp_effort(float control_effort) {
  return std::max(lower_effort_limit_, std::min(control_effort, upper_effort_limit_));
}

void DifferentialDriveController::left_wheel_control_callback(const std_msgs::Float64::ConstPtr& msg) {
  wheel_effort_.left = clamp_effort(wheel_effort_.left + round(msg->data * 100) / 100.0);
}

void DifferentialDriveController::right_wheel_control_callback(const std_msgs::Float64::ConstPtr& msg) {
  wheel_effort_.right = clamp_effort(wheel_effort_.right + round(msg->data * 100) / 100.0);
}

void DifferentialDriveController::publish_state(wheel_data wheel_velocity) {
  std_msgs::Float64 msg;
  msg.data = roundf(wheel_velocity.left * 100) / 100.0;
  left_wheel_state_pub_.publish(msg);
  msg.data = roundf(wheel_velocity.right * 100) / 100.0;
  right_wheel_state_pub_.publish(msg);
}