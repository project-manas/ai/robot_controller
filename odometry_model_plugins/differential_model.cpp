#include "pluginlib/class_list_macros.h"
#include "odometry_model/differential_model.h"

PLUGINLIB_EXPORT_CLASS(differential_model::DifferentialModel, odometry_model::OdometryModel)


using namespace differential_model;

void DifferentialModel::initialize(ros::NodeHandle *nh, ros::NodeHandle *private_nh) {
  nh_ = nh;
  private_nh_ = private_nh;
  private_nh_->param("odometry_topic", odometry_topic_, std::string("odom"));
  private_nh_->param("odometry_frame", odometry_frame_, std::string("odom"));
  private_nh_->param("robot_frame", robot_frame_, std::string("base_link"));
  private_nh_->param("axle_track", axle_track_, 1.0);

  odometry_pub_ = nh_->advertise<nav_msgs::Odometry>(odometry_topic_, 1000);
}

void DifferentialModel::update(std::string data, ros::Time last_update_time) {
  deserialize(data);
  last_update_time_ = last_update_time;
  if(wheel_vel_.left < 2.0 && wheel_vel_.left > -2.0 && wheel_vel_.right < 2.0 && wheel_vel_.right > -2.0)
    forward_kinematics();
}

void DifferentialModel::deserialize(std::string data)
{
  // Handle endianess
  std::memcpy(&wheel_vel_, data.c_str(), 8);
}

void DifferentialModel::forward_kinematics()
{
  auto current_time = ros::Time::now();
  ros::Duration time_elapsed = current_time - last_update_time_;
  last_update_time_ = current_time;
  double speed = (wheel_vel_.right + wheel_vel_.left) / 2.0;
  double angular_velocity = ((wheel_vel_.right - wheel_vel_.left) / axle_track_);
  double heading = angular_velocity * time_elapsed.toSec();
  heading = constrain_angle(heading);

  Odometry odom = Odometry();
  odom.velocity.x = speed; // * cos(current_odom_.position.yaw + heading);
  odom.velocity.y = 0.0; //speed * sin(current_odom_.position.yaw + heading);
  odom.velocity.z = 0.0;
  odom.velocity.yaw = angular_velocity;
  odom.position.x = current_odom_.position.x + speed * cos(current_odom_.position.yaw + heading) * time_elapsed.toSec();
  odom.position.y = current_odom_.position.y + speed * sin(current_odom_.position.yaw + heading) * time_elapsed.toSec();
  odom.position.z = 0.0;
  odom.position.yaw = constrain_angle(current_odom_.position.yaw + heading);
  current_odom_ = odom;
  publish_odometry(odom, current_time);
}

void DifferentialModel::publish_odometry(Odometry odom, ros::Time current_time)
{
  nav_msgs::Odometry msg;
  tf::Quaternion q = tf::createQuaternionFromRPY(0, 0, odom.position.yaw);
  tf::quaternionTFToMsg(q, msg.pose.pose.orientation);
  msg.header.frame_id = odometry_frame_;
  msg.header.stamp = current_time;
  msg.child_frame_id = robot_frame_;
  msg.pose.pose.position.x = odom.position.x;
  msg.pose.pose.position.y = odom.position.y;
  msg.pose.pose.position.z = odom.position.z;
  msg.twist.twist.linear.x = odom.velocity.x;
  msg.twist.twist.linear.y = odom.velocity.y;
  msg.twist.twist.linear.z = odom.velocity.z;
  msg.twist.twist.angular.x = 0.0;
  msg.twist.twist.angular.y = 0.0;
  msg.twist.twist.angular.z = odom.velocity.yaw;
  msg.twist.covariance[0] = 0.001; 
  msg.twist.covariance[7] = 0.001; 
  msg.twist.covariance[35] = 0.05; 
  odometry_pub_.publish(msg);
}

/*********** Helper Functions **********/

double DifferentialModel::constrain_angle(double x)
{
  x = fmod(x, 2.0 * M_PI);
  if (x < 0)
    x += 2.0 * M_PI;
  return x;
}