/*************************************
*  Copyright (c) 2019, Project MANAS
*  Author: Shrijit Singh
*************************************/

#include "robot_controller/robot_controller.h"

using namespace robot_controller;

RobotController::RobotController(ros::NodeHandle *node, serial::Serial *serial) :
    nh_(node),
    private_nh_("~"),
    controller_loader_("robot_controller", "controller::Controller"),
    odometry_model_loader_("robot_controller", "odometry_model::OdometryModel"),
    serial_(serial)
{
  private_nh_.param("rate", rate_, 30.0);
  private_nh_.param("data_size", data_size_, 8);
  private_nh_.param("start_delimitter", start_delimitter_, std::string("["));
  private_nh_.param("end_delimitter", end_delimitter_, std::string("]"));

  load_plugins();
}

void RobotController::load_plugins() {
  if (private_nh_.hasParam("controller_plugin"))
  {
    XmlRpc::XmlRpcValue controller_plugin;
    private_nh_.getParam("controller_plugin", controller_plugin);
    std::string pname = static_cast<std::string>(controller_plugin[0]["name"]);
    std::string ptype = static_cast<std::string>(controller_plugin[0]["type"]);
    ROS_INFO("Using controller plugin \"%s\"", pname.c_str());

    controller_ = controller_loader_.createInstance(ptype);
    controller_->initialize(nh_, &private_nh_);
  } else {
    ROS_ERROR("Failed to load controller plugin");
  }

  if (private_nh_.hasParam("odometry_model_plugin"))
  {
    XmlRpc::XmlRpcValue odometry_model_plugin;
    private_nh_.getParam("odometry_model_plugin", odometry_model_plugin);
    std::string pname = static_cast<std::string>(odometry_model_plugin[0]["name"]);
    std::string ptype = static_cast<std::string>(odometry_model_plugin[0]["type"]);
    ROS_INFO("Using odometry model plugin \"%s\"", pname.c_str());

    odometry_model_ = odometry_model_loader_.createInstance(ptype);
    odometry_model_->initialize(nh_, &private_nh_);
  } else {
    ROS_ERROR("Failed to load odometry model plugin");
  }
}

void RobotController::run() {
  ros::Rate loop_rate(rate_);
  auto last_update_time = ros::Time::now();
  this->serial_parser = new serial::SerialParser(*serial_, start_delimitter_, end_delimitter_, 8);
  std::string parsed_string;
  auto *buffer = (uint8_t *) malloc(sizeof(uint8_t) * data_size_);
  auto buffer_length = (size_t) data_size_;

  while (ros::ok())
  {
    parsed_string = this->serial_parser->get_parsed_string();
    if (!parsed_string.empty())
    {
      odometry_model_->update(parsed_string, last_update_time);
      controller_->get_effort(parsed_string, buffer, buffer_length, last_update_time);
      // float *a = (float*) malloc(sizeof(float) * 2);
      // memcpy(a, buffer, sizeof(float)*2);
      // a[0] += 1;
      // a[1] += 1;
      // memcpy(buffer, a, sizeof(float)*2);
      // std::cout<<"*****"<<a[0]<<"  "<<a[1]<<std::endl;
      serial_->write(buffer, buffer_length);
      last_update_time = ros::Time::now();
    }

    ros::spinOnce();
    loop_rate.sleep();
  }
}
