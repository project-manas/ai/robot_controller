/*************************************
*  Copyright (c) 2019, Project MANAS
*  Author: Shrijit Singh
*************************************/

#ifndef ROBOT_CONTROLLER_ODOMETRY_MODEL_H
#define ROBOT_CONTROLLER_ODOMETRY_MODEL_H

#include "ros/ros.h"

class Odometry {
 public:
  class Position {
   public:
    double x;
    double y;
    double z;
    double roll;
    double pitch;
    double yaw;
  } position;
  class Velocity {
   public:
    double x;
    double y;
    double z;
    double roll;
    double pitch;
    double yaw;
  } velocity;
};

std::ostream& operator<<(std::ostream& strm, const Odometry& odom) {
  return strm << "Odometry("
              << "\n Position:\n"
              << "  x: " << odom.position.x << "\n  y: " << odom.position.y
              << "\n  z: " << odom.position.z
              << "\n  roll: " << odom.position.roll
              << "\n  pitch: " << odom.position.pitch
              << "\n  yaw: " << odom.position.yaw << "\n Velocity:\n"
              << "  x: " << odom.velocity.x << "\n  y: " << odom.velocity.y
              << "\n  z: " << odom.velocity.z
              << "\n  roll: " << odom.velocity.roll
              << "\n  pitch: " << odom.velocity.pitch
              << "\n  yaw: " << odom.velocity.yaw << "\n)\n";
}

namespace odometry_model {

class OdometryModel {
 public:
  virtual void initialize(ros::NodeHandle* nh, ros::NodeHandle* private_nh) = 0;
  virtual ~OdometryModel() = default;
  virtual void update(std::string data, ros::Time last_update_time) = 0;

 protected:
  OdometryModel() = default;
};
};

#endif  // ROBOT_CONTROLLER_ODOMETRY_MODEL_H
