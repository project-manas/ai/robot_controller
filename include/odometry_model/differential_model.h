/*************************************
*  Copyright (c) 2019, Project MANAS
*  Author: Shrijit Singh
*************************************/

#ifndef DIFFERENTIAL_MODEL_H
#define DIFFERENTIAL_MODEL_H

#include "nav_msgs/Odometry.h"
#include "odometry_model/odometry_model.h"
#include "ros/ros.h"
#include "serial/serial.h"
#include "std_msgs/Float64.h"
#include "tf/transform_datatypes.h"

namespace differential_model {

class DifferentialModel : public odometry_model::OdometryModel {
 private:
  ros::NodeHandle* nh_;
  ros::NodeHandle* private_nh_;
  ros::Publisher odometry_pub_;

  std::string odometry_topic_;
  std::string odometry_frame_;
  std::string robot_frame_;
  double axle_track_;
  ros::Time last_update_time_;
  Odometry current_odom_;
  struct wheel_data {
    float left;
    float right;
  } wheel_vel_;

  void deserialize(std::string data);
  void forward_kinematics();
  void publish_odometry(Odometry odom, ros::Time current_time);
  double constrain_angle(double x);

 public:
  void initialize(ros::NodeHandle* nh, ros::NodeHandle* private_nh) override;
  void update(std::string data, ros::Time last_update_time) override;
};
}  // namespace differential_model

#endif  // DIFFERENTIAL_MODEL_H
