/*************************************
*  Copyright (c) 2019, Project MANAS
*  Author: Shrijit Singh
*************************************/

#ifndef ROBOT_CONTROLLER_ROBOT_CONTROLLER_H
#define ROBOT_CONTROLLER_ROBOT_CONTROLLER_H

#include "controller/controller.h"
#include "odometry_model/odometry_model.h"
#include "pluginlib/class_loader.h"
#include "ros/ros.h"
#include "serial/serial.h"
#include "serial_parser/serial_parser.h"

namespace robot_controller {
class RobotController {
 private:
  ros::NodeHandle *nh_;
  ros::NodeHandle private_nh_;
  serial::Serial *serial_;
  pluginlib::ClassLoader<controller::Controller> controller_loader_;
  pluginlib::ClassLoader<odometry_model::OdometryModel> odometry_model_loader_;
  boost::shared_ptr<controller::Controller> controller_;
  boost::shared_ptr<odometry_model::OdometryModel> odometry_model_;
  serial::SerialParser *serial_parser;

  double rate_;
  int data_size_;
  std::string start_delimitter_;
  std::string end_delimitter_;

 public:
  RobotController(ros::NodeHandle *node, serial::Serial *serial);
  void load_plugins();
  void run();
};
}

#endif  // ROBOT_CONTROLLER_ROBOT_CONTROLLER_H
