/*************************************
*  Copyright (c) 2019, Project MANAS
*  Author: Shrijit Singh
*************************************/

#ifndef ROBOT_CONTROLLER_DIFF_DRIVE_CONTROLLER_H
#define ROBOT_CONTROLLER_DIFF_DRIVE_CONTROLLER_H

#include "controller/controller.h"
#include "std_msgs/Float64.h"

namespace diff_drive_controller {

class DifferentialDriveController : public controller::Controller {
 private:
  ros::NodeHandle *nh_;
  ros::NodeHandle *private_nh_;
  ros::Publisher left_wheel_state_pub_;
  ros::Publisher right_wheel_state_pub_;
  ros::Subscriber left_wheel_control_sub_;
  ros::Subscriber right_wheel_control_sub_;

  float upper_effort_limit_;
  float lower_effort_limit_;

  struct wheel_data {
    float left, right;
    wheel_data() : left(0.0), right(0.0) {}
  } wheel_velocity_, wheel_effort_;

  void setup_pid_controller();
  void deserialize(std::string data);
  void serialize(wheel_data control_effort, uint8_t *buffer,
                 size_t buffer_length);
  void publish_state(wheel_data wheel_velocity);
  float clamp_effort(float control_effort);

  void left_wheel_control_callback(const std_msgs::Float64::ConstPtr &msg);
  void right_wheel_control_callback(const std_msgs::Float64::ConstPtr &msg);

 public:
  void initialize(ros::NodeHandle *nh, ros::NodeHandle *private_nh) override;
  void get_effort(std::string data, uint8_t *buffer, size_t buffer_length,
                  ros::Time last_update_time) override;
};
}

#endif  // ROBOT_CONTROLLER_DIFF_DRIVE_CONTROLLER_H
