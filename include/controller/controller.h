/*************************************
*  Copyright (c) 2019, Project MANAS
*  Author: Shrijit Singh
*************************************/

#ifndef ROBOT_CONTROLLER_CONTROLLER_H
#define ROBOT_CONTROLLER_CONTROLLER_H

#include "ros/ros.h"

namespace controller {
class Controller {
 public:
  virtual void initialize(ros::NodeHandle *nh, ros::NodeHandle *private_nh) = 0;
  virtual ~Controller() = default;
  virtual void get_effort(std::string data, uint8_t *buffer,
                          size_t buffer_length, ros::Time last_update_time) = 0;

 protected:
  Controller() = default;
};
};

#endif  // ROBOT_CONTROLLER_CONTROLLER_H
